﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = new List<Student>()
            {
                new Student()
                {
                    FirstName = "Malle",
                    LastName = "Maasikas",
                    Age = 13
                },
                new Student()
                {
                    FirstName = "Peeter",
                    LastName = "Peet",
                    Age = 23
                },
                new Student()
                {
                    FirstName = "Kaie",
                    LastName = null,
                    Age = 23
                },
                new Student()
                {
                    FirstName = "Jaana",
                    LastName = "Jalgratas",
                    Age = 40
                }
            };
            // SELECT * FROM Student
            var allStudents = (from student in students
                              select student
                              ).ToList();

            // Lamda süntaks
            allStudents = students.ToList();

            
            // SELECT * FROM Student
            // WHERE Age > 20
            // ORDER BY FirstName DESC

            // QUERY süntaks
            var oldStudents = (from student in students
                               where student.Age > 20
                               orderby student.Age ascending,
                               student.FirstName descending
                               select student).ToList();

            // Lamda sütanks
            oldStudents = students
                .Where(x => x.Age > 20)
                .OrderBy(x => x.Age)
                .ThenByDescending(x => x.FirstName)
                .ToList();

            // Sama asi foreachiga
            var youngStudents = new List<Student>();
            foreach (var student in students)
            {
                if(student.Age < 20)
                {
                    youngStudents.Add(student);
                }
            }


            // Täienda nii, et ainult nendel õpilastel, kellel on eesnimes 5 tähte
            List<string> initials = (from student in students
                           where student.FirstName.Length == 5
                           select student.FirstName[0] + " " + student.LastName[0]
                           ).ToList();

            initials = students
                 .Where(x => x.FirstName.Length == 5)
                 .Select(x => x.FirstName[0] + " " + x.LastName.Substring(0, 1))
                 .ToList();
            
            // Täisarvude massiivist anna mulle kõik paariarvud
            // a % 2 == 0
            int[] numbers = new int[] { 2, 5, 4, 8, 6 };
            List<int> equalNumbers = (from number in numbers
                                where number % 2 == 0
                                select number).ToList();

            equalNumbers = numbers.Where(x => x % 2 == 0).ToList();

            // Anna mulle õpilaste nimikiri, kus perekonnanimes kõik a, e, i on asendatud o-ga
            var changedStudents = (from student in students
                                   select 
                                   new
                                   {
                                       FirstName = student.FirstName,
                                       LastName = student.LastName != null ? student.LastName
                                                    .Replace('a', 'o')
                                                    .Replace('i', 'o')
                                                    .Replace("e", "o") : null,
                                       Age = student.Age
                                   }).ToList();

            //changedStudents = students.Select(x => new Student()
            //{
            //    FirstName = x.FirstName,
            //    LastName = x.LastName != null ? x.LastName
            //                                        .Replace('a', 'o')
            //                                        .Replace('i', 'o')
            //                                        .Replace("e", "o") : null,
            //    Age = x.Age
            //}).ToList();

            Console.WriteLine("{0} {1}", changedStudents[0].FirstName, changedStudents[0].)
            int a = 4;

            int b;
            if(a < 10)
            {
                b = 1;
            }
            else
            {
                b = 2;
            }

            b = a < 10 ? 1 : 2;

            string text;
            if(students[2].LastName != null)
            {
                text = students[2].LastName;
            }
            else
            {
                text = "tühi";
            }

            text = students[2].LastName ?? "tühi";

            // Anna mulle õpilane, Malle Maasikas

            Student oneStudent = students
                .Where(x => x.FirstName == "Malle" && x.LastName == "Maasikas")
                .FirstOrDefault();

            // First ja FirstOrDefault vahe on see,
            // et First annab exceptioni, kui sellist tulemuste ei leitud
            // aga FirstOrDefault tagastab null või (int, double puhul 0. Booleani puhul false);
            // Enamasti kasutatakse FirstOrDefault

          
            // Maksimum vanus
            var max = students.Max(x => x.Age);

            // Anna mulle vanim õpilane

            oneStudent = students.Where(x => x.Age == students.Max(y => y.Age)).FirstOrDefault();
            oneStudent = students.OrderByDescending(x => x.Age).FirstOrDefault();
            // Anna mulle õpilaste keskmine vanus
            var averageAge = students.Average(x => x.Age);

            Console.ReadLine();
        }
    }
}
