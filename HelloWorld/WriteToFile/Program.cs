﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace WriteToFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] lines = new string[] {
                "Elas metsas Mutionu",
                "keset kuuski noori vanu",
                "kadak põõsa juure all",
                "eluruum tal sügaval" };

            // Otsib faili MinuFail.txt kaustast, kus asub minug exe fail
            // FileMode.Append kirjutab vanale failile juurde
            // FileMode.Create loob alati uue faili (vana sisu kirjutakse üle)
            FileStream fileStream = null;
            StreamWriter streamWriter = null;

            try
            {
                fileStream = new FileStream("MinuFail.txt", FileMode.Create, FileAccess.Write);
                streamWriter = new StreamWriter(fileStream);

                for (int i = 0; i < lines.Length; i++)
                {
                    streamWriter.WriteLine(lines[i]);
                }

            }
            finally
            {
                if(streamWriter != null)
                {
                    streamWriter.Close();
                }
                if(fileStream != null)
                {
                    fileStream.Close();
                }
                
            }
        
            
            //  File.WriteAllLines("MinuFail.txt", lines);
            File.WriteAllLines("MinuFail.txt", lines); // Massiivi kirjutamiseks
            File.WriteAllText("MinuFail.txt", "Elas metsas Mutionu\r\nkeset kuuski noori vanu\r\nkadak põõsa juure all\r\neluruum tal sügaval");

        }
    }
}
