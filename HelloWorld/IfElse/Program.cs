﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IfElse
{
    class Program
    {
        static void Main(string[] args)
        {
            string myName = "Lauri";
            Console.WriteLine("Mis on Sinu nimi?");
            string name = Console.ReadLine();
            // "lauri" -> "LAURI"

            // "LAURI" == "LAURI"

            //if(name.Equals(myName, StringComparison.InvariantCultureIgnoreCase))
            if(name.ToUpper() == myName.ToUpper())
            {
                Console.WriteLine("Tere Lauri");

                Console.WriteLine("Mis on sinu vanus?");

                int age = Convert.ToInt32(Console.ReadLine());
                if(age < 18)
                {
                    Console.WriteLine("Sisse ei pääse");
                }
                else
                {
                    Console.WriteLine("Tere tulemast klubisse");
                }
                // Küsi vanust ning kui vanus on alaealine, siis 
                // kirjuta, et sisse ei pääse
                // muuljuhul, kirjuta tere tulemast klubisse
            }
            else
            {
                Console.WriteLine("Ma ei tunne sind");

                Console.WriteLine("Mis on Sinu perekonnanimi?");
                string lastName = Console.ReadLine();
                if (lastName == "Mattus" || lastName == "mattus")
                {
                    Console.WriteLine("Tere Lauri Mattus");
                }
                else
                {
                    Console.WriteLine("Ma ikkagi ei tunne sind");
                }
                    // Küsib perekonnanime ning kui perekonnanimi
                    // klapib, siis ütle tere Lauri Mattus. Muuljuhul,
                    // ütleb, ma ikkagi ei tunne sind
            }

            Console.ReadLine();
        }
    }
}
