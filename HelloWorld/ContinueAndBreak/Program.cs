﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContinueAndBreak
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                Console.WriteLine("Head\vAega");
                
                Console.WriteLine("Kas soovid jätkata? Vasta j/e");
                if(Console.ReadLine().ToLower() != "j")
                {
                    break;
                }
            }

            for (int i = -20; i <= 40; i++)
            {
                if(i >= 10 || i <= 20)
                {
                    continue;
                }
                Console.WriteLine(i);
            }

            Console.ReadLine();
        }
    }
}
