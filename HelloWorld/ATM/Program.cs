﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM
{
    class Program
    {
        // See on klassi muutuja, kehtib terve klassi piires ehk kõikides klassi meetodites
        
        static string pin;
        static double balance;

        static void Main(string[] args)
        {
            // Faili kirjutamist ja failist lugemist teha alati 
            // võimalikult harva
            pin = LoadPin();
            balance = LoadBalance();

            if(!CheckPinCode())
            {
                return;
            }

            Console.WriteLine("Vali toiming:");
            Console.WriteLine("a) Sularaha sissemakse");
            Console.WriteLine("b) Sularaha väljamakse");
            Console.WriteLine("c) Kontojääk");
            Console.WriteLine("d) Katkesta");
            Console.WriteLine("e) Muuda pin");
   

            string answer = Console.ReadLine();
            switch (answer)
            {
                case "a":
                    Console.WriteLine("Sisesta summa:");
                    int amount = Convert.ToInt32(Console.ReadLine());
                    balance = balance + amount;
                    SaveBalance(balance);
                    Console.WriteLine("Sisestati {0}. Uus kontojääk on {1}", amount, balance);
                    break;
                    
                case "b":
                    Console.WriteLine("Vali summa:");
                    Console.WriteLine("a) 5");
                    Console.WriteLine("b) 10");
                    Console.WriteLine("c) 20");
                    Console.WriteLine("d) 100");
                    Console.WriteLine("e) Muu summa");
                    answer = Console.ReadLine();
                    switch(answer)
                    {
                        case "a":
                            balance = balance - 5;
                            SaveBalance(balance);
                            Console.WriteLine("Väljastati 5. Uus kontojääk on {0}", balance);
                            break;
                    }

                    break;
                case "c":
                   
                    Console.WriteLine("Kontojääk on {0}", balance);
         
                    break;


                default:
                   
                   break;
                    
                    
            }
            // kas vana õige
            // kas uus ja teine kord uus on võrdesd
            // 
            SavePin(pin);


            //balance = balance - 10;
            //SaveBalance(balance);
            Console.ReadLine();

        }

        private static bool CheckPinCode()
        {
            for (int i = 1; i <= 3; i++)
            {
                Console.WriteLine("Sisesta PIN kood:");
                string enteredPin = Console.ReadLine();
                if (!IsPinCorrect(enteredPin))
                {
                    Console.WriteLine("Vale PIN");
                    if (i < 3)
                    {
                        Console.WriteLine("Proovi uuesti.");
                    }
                    else
                    {
                        Console.WriteLine("Kaart on konfiskeeritud");
                        Console.ReadLine();
                        // Return lõpetab meetodi töö

                        return false; 
                    }
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        static string LoadPin()
        {
            // Loeb failist
            return File.ReadAllText("Pin.txt").TrimEnd(new char[] { '\r', '\n', ' '});
            //string[] lines = File.ReadAllLines("Pin.txt");
            //return lines[0];
        }

        static void SavePin(string newPin)
        {
            //string[] lines = new string[] { newPin };
            //"Peeter ja Jaan ; Juhan, Malle".Split(new string[] { " ja ", ", ", ";" }, StringSplitOptions.None);
            // Salvestatakse pin faili

            //File.WriteAllLines("Pin.txt", lines);

            File.WriteAllText("Pin.txt", newPin);
            pin = newPin;
            //File.WriteAllLines("Pin.txt", new string[] { pin };)
        }

        static double LoadBalance()
        {

            // Loeb failist
            return Convert.ToDouble(File.ReadAllText("Balance.txt"));
        }

        static void SaveBalance(double newBalance)
        {
            File.WriteAllText("Balance.txt", Convert.ToString(newBalance));
            balance = newBalance;
            // Salvestatakse faili
        }

        static bool IsPinCorrect(string enteredPin)
        {
            //return enteredPin == pin;
            if(enteredPin == pin)
            {
                return true;
            }

            return false;

        }
    }
}
