﻿using Inheritance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingUnboxing
{
    class Program
    {
        static void Main(string[] args)
        {
            // Boxing ja unboxing on resursinõudlik protsess

            int a = 3;

            // object tüüpi muutujasse
            // saab panna ükskõik mis .NET defineeritud muutujat
            // boxing
            object b = a;

            // unboxing
            var c = (int)b;

            Console.WriteLine(b);


            Print(123);
            Print(false);
            Print("Tere");
            Print(12.75);

            Cat cat = new Cat();
            cat.Name = "Miisu";

            object e = cat; // boxing
            Cat sameCat = (Cat)e; // unboxing

            // tehakse samamoodi boxing
            object[] objects = new object[] { 2, "tere", false, cat };

            // unboxing
            Console.WriteLine((int)objects[0]);
            Console.WriteLine((string)objects[1]);
            Console.WriteLine(((Cat)objects[3]).Name);

            Animal firstAnimal = new Cat() { Name = "Kitu" };
            Animal secontAnimal = new Lion() { Name = "Liisu" };

            List<Animal> animals = new List<Animal>();
            animals.Add(new Cat() { LivesRemaining = 8, Name = "Kati" });
            animals.Add(new Dog() { FavouriteToy = "Karu", Name = "Peeter" });
            animals.Add(firstAnimal);
            animals.Add(secontAnimal);

            foreach (var animal in animals)
            {
                animal.Eat();
            }

            Dog thirdAnimal = (Dog)animals[1];
            Console.WriteLine(thirdAnimal.FavouriteToy);

            // Saab teha teisendamist kõikide tüüpide vahel
            // mis on pärilusega seotud


            // Kassi ei saa teisendada koeraks,
            // sest nad ei pärine teineteisest
            //Dog dog = (Dog)cat;
            Wolf wolf = new Wolf();
            WildAnimal wildAnimal = wolf;
            Wolf secondWolf = (Wolf)wildAnimal;
            secondWolf.Howl();

            Carnivore carnivore = (Carnivore)wildAnimal;
            Wolf thirdWolf = (Wolf)carnivore;
            WildAnimal secondWildAnimal = carnivore;



            Console.ReadLine();
        }

        static void Print(object obj)
        {
            if(obj is string)
            {
                Console.WriteLine("See string on {0}", (string)obj);
            }
            else if(obj is bool)
            {
                Console.WriteLine("See boolen on {0}", (bool)obj);
            }
            else if(obj is int)
            {
                Console.WriteLine("See int on {0}", (int)obj);
            }
            else
            {
                Console.WriteLine("Selle {0}'ga ei oska ma midagi peale hakata", obj.GetType().Name);
            }
        }
    }
}
