﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            // Peeter Mari Kaie Paul
            // anna esimene
            // Mari Kaie Paul
            // lisandub Jüri
            // Mari Kaie Paul Jüri
            // anna esimene
            // Kaie Paul Jüri

            Queue<string> queue = new Queue<string>();
            queue.Enqueue("Peeter");
            queue.Enqueue("Mari");
            queue.Enqueue("Kaie");
            queue.Enqueue("Paul");

            // Tsükkel mingi masiivi või muu kollektsiooni läbimiseks
            // Elemente tsükli sees muuta ei saa
            // Iga queues oleva stringi kohta kohta tee muutuja item
            // Ehk iga tsüki korduse sees on string item võrdne järgneva eleendiga

            foreach (string item in queue)
            {
                Console.WriteLine(item);
            }

            string currentClient = queue.Dequeue();

            Console.WriteLine("Teenindatakse klienti {0}", currentClient);

            Console.WriteLine();

            foreach (string item in queue)
            {
                Console.WriteLine(item);
            }

            queue.Enqueue("Jüri");

            Console.WriteLine();

            foreach (string item in queue)
            {
                Console.WriteLine(item);
            }

            string nextClient = queue.Peek();

            if(nextClient == "Mari")
            {
                string name = queue.Dequeue();
                queue.Enqueue(name);
            }

            Console.WriteLine();

            foreach (string item in queue)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();

        }
    }
}
