﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            float a = 130;
            float b = 9;
            Console.WriteLine(a / b);

            double c = 130;
            double e = 9;
            Console.WriteLine(c / e);

            a = 1300000;
            b = 230000000;
            c = 1300000;
            e = 230000000;
            Console.WriteLine(a * b);
            Console.WriteLine(c * e);

            decimal f = 130;
            decimal g = 9;

            Console.WriteLine(f / g);

            // Pean veenduma, kas e mahub i sisse
            decimal i = (decimal)e;
            // Pean veenduma, et ma täpsuses ei kaota
            double j = (double)i;

            // Toimub implicit teisendus, 
            // sest a mahub ilusti ka sisse
            double k = a;

            float l = (float)i;
            Single m = (float)j;

            Int32 n = 22;

            // short = Int16
            // int = Int32
            // long = int64
            // float = Single
            // double = Double
            // string = String

            // Tehetel täisarvu ja reaalarvu vahel on tulemus alati
            // antud reaalarvu tüüp

            Console.WriteLine((n * l) + " " + (n * l).GetType());
            Console.WriteLine((n * j) + " " + (n * j).GetType());

            // Täisarvust reaalarvuks toimub alati implicit (iseneslik) teisenemine
            long o = 5;
            double p = o;
            byte q = 2;
            decimal r = q;
            byte s = (byte)r;

            Console.ReadLine();
        }
    }
}
