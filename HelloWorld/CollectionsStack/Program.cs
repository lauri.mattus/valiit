﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsStack
{
    class Program
    {
        static void Main(string[] args)
        {
            // "Tõde ja õigus" "Pipi Pikksukk" "Kroonika"
            // Lisan hunnikusse raamatu "Kalevipoeg"
            // "Tõde ja õigus" "Pipi Pikksukk" "Kroonika" "Kalevipoeg"
            // Võtan hunnikust raamatu "Kalevipoeg"
            // "Tõde ja õigus" "Pipi Pikksukk" "Kroonika"
            // Võtan hunnikust raamatu "Kroonika"
            // "Tõde ja õigus" "Pipi Pikksukk"
            // Võtan hunnikust raamatu "Pipi Pikksukk"
            // "Tõde ja õigus"

            Stack<string> stack = new Stack<string>();
            stack.Push("Tõde ja õigus");
            stack.Push("Pipi Pikksukk");
            stack.Push("Kroonika");

            foreach (string book in stack)
            {
                Console.WriteLine(book);
            }

            string topBook = stack.Pop();
            Console.WriteLine($"Ülemine raamat oli {topBook}");

            Console.WriteLine();
            foreach (string book in stack)
            {
                Console.WriteLine(book);
            }

            topBook = stack.Peek();
            Console.WriteLine($"Ülemine raamat on nüüd {topBook}");

            Console.WriteLine();
            foreach (string book in stack)
            {
                Console.WriteLine(book);
            }

            Console.ReadLine();

        }
    }
}
