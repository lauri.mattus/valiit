﻿// Annab ligipääsu kõigile klassidele, mis asuvad System nimeruumis
using System;

// Kõik klasside nimed, mis asuvad Helloworld nimeruumis sees 
// kehtivad selle nimeruumi piires
namespace HelloWorld
{

    // Klass nimega Program
    class Program
    {
        // Staatiline meetod 
        // void -> meetod ei tagasta midagi
        // Main -> eriline meetod, millest programm alustab tööd
        // string[] args -> meetodi parameetrid (argumendid)
        static void Main(string[] args)
        {


            // Kutsutake välja Console klassi meetod nimega WriteLine()
            // "Tere Maailm" -> Meetodi WriteLine() parameeter ehk tekst, mida välja printida
            Console.WriteLine("Tere Kuidas läheb?");
            // Ootab kasutajalt vastust ehk teksti, mis lõppeb enteriga
            Console.ReadLine();
            // Kutsub välja Printer nimeruumist klassi Writer meetodi Write()
            Printer.Writer.Write();
            // Kutsub välja File nimeruumist klassi Writer meetodi Write()
            File.Writer.Write();
            // Kutsub välja Database nimeruumist klassi Writer meetodi Write()
            Database.Writer.Write();
        }

        static void Convert()
        {
        }
    }

    
}
namespace Hello
{
    public class Program
    {
        public static void Convert()
        {
        }
    }
}

namespace Printer
{
    public class Writer
    {
        public static void Write()
        {
        }
    }
}

namespace Database
{
    public class Writer
    {
        public static void Write()
        {
        }
    }
}

namespace File
{
    public class Writer
    {
        public static void Write()
        {
        }
    }
}

