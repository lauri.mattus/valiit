﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conditions
{
    class Program
    {
        static void Main(string[] args)
        {
            // Seadista programmi keeleks USA inglise keel
            CultureInfo.CurrentCulture = new CultureInfo("en-US");

            Console.WriteLine("Sisesta palun 1 number");
            string answer = Console.ReadLine();

            // asendab tekstis kõik komad punktidega
            answer = answer.Replace(",", ".");

            double a;

            // Kontrolli, kas programmi keel on Eesti eesti keel
            if (CultureInfo.CurrentCulture.Name == "et-EE")
            {
                // kasuta , reaalarvus
                a = Convert.ToDouble(answer);
            }
            else
            {
                // kasuta . reaalarvus
                // lisaparameeter konverteerimise keelena
                a = Convert.ToDouble(answer, new CultureInfo("en-US"));
            }

            Console.WriteLine(a);
           

            if(a == 3.24)
            {
                Console.WriteLine("Arv on võrdne kolmega");
            }

            if(a != 4)
            {
                Console.WriteLine("Arv ei ole võrdne neljaga");
            }

            if(a > 2)
            {
                Console.WriteLine("Arv on suurem kahest");
            }

            if (a < 6)
            {
                Console.WriteLine("Arv on väiksem kuuest");
            }

            // Kui arv on suurem võrdne 5ga
            if (a >= 5)
            {
                Console.WriteLine("Arv on suurem võrdne 5ga");
            }

            // Kui arv on väiksem kui 2 või suurem kui 8
            if (a < 2 || a > 8 )
            {
                Console.WriteLine("Arv on väiksem kui 2 või suurem kui 8");
            }
            // || -> VÕI
            // && -> JA

            // Kui arv on 2 ja 8 vahel
            if (a > 2 && a < 8)
            {
                Console.WriteLine("Arv  on 2 ja 8 vahel");
            }

            // Kui arv on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui 10
            if ((a > 2 && a < 8) || (a > 5 && a < 8) || a > 10)
            {
                Console.WriteLine(" on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui 10");
            }


            Console.ReadLine();
        }
    }
}
