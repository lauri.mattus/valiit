﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsList
{
    class Program
    {
        static void Main(string[] args)
        {
            // Massiiv 5 arvust 1, 3, 0, 5, -4
            // Kustuta massivist esimene number

            int[] numbers = new int[5];


            for (int i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine(numbers[i]);
            }

            // Massiiv 5 sõnast "kala", "auto", "", "maja", "telefon"
            // Kustuta massivist esimene sõna

            string[] words = new string[] { "kala", "auto", "", "maja", "telefon" };

            // null on tühihulk, tühjus ehk siis mälu pole eraldatud
            words[0] = null;
            words[0] = words[1];
            words[1] = "vesi";

            for (int i = 0; i < words.Length; i++)
            {
                if(words[i] == null)
                {
                    Console.WriteLine("Tühi");
                }
                else
                {
                    Console.WriteLine(words[i]);
                }
              
            }
            int c;
            string word;

            Console.WriteLine();
            List<int> numbersList = new List<int>() { 1, 3, 22, -2 };

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            numbersList.Add(5);

            Console.WriteLine();
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            numbersList.AddRange(new int[] { 1, 2, 3 });

            Console.WriteLine();
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            numbersList.Remove(1);
            Console.WriteLine();
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            numbersList.RemoveAt(2);

            Console.WriteLine();
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            numbersList.RemoveRange(2, 3);

            Console.WriteLine();
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            numbersList.Insert(1, 1);

            Console.WriteLine();
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            numbersList.InsertRange(2, new int[] { 1, -2, 33 });

            Console.WriteLine();
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }


            //if (numbersList.IndexOf(111) != -1)
            if (numbersList.Contains(111))
            {
                Console.WriteLine("Nimekirjas on olemas number 1");
            }

            Console.WriteLine("Numbri 33 indeks on {0}", numbersList.IndexOf(330));

            numbersList.Reverse();


            Console.WriteLine();
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            numbersList.Sort();

            Console.WriteLine();
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }

            List<string> wordsList = new List<string>();

            List<double> realNumbers = new List<double>();

            ArrayList arrayList = new ArrayList() { 3, "maja", 'c', true, 3.45 };
            arrayList.Add(2);
            arrayList.Add("tere");

            int e = -8;
            string f = "kalamees";
            bool isGood = false;

            arrayList.Insert(1, e);
            arrayList.Insert(1, f);
            arrayList.Insert(1, isGood);

            Console.WriteLine();

            for (int i = 0; i < arrayList.Count; i++)
            {
                Console.WriteLine(arrayList[i]);
            }

            // is on tüübi kontroll
            // kui element kohal 4 on string, siis on tõene
            if(arrayList[4] is string)
            {
                word = (string)arrayList[4];
                Console.WriteLine(word.ToUpper());
            }

            else if(arrayList[4] is int)
            {
                int g = (int)arrayList[4];
                g++;
                Console.WriteLine(g);
            }

            //arrayList.Remove
            //arrayList.Insert

            if (arrayList[4] is string)
            {
                word = (string)arrayList[4];
                Console.WriteLine(word.ToUpper());
            }

            else if (arrayList[4] is int)
            {
                int g = (int)arrayList[4];
                g++;
                Console.WriteLine(g);
            }

            HashSet<int> hashSet = new HashSet<int>() { 1, 2, 3 };

            hashSet.Add(1);
            hashSet.Add(4);
            hashSet.Add(4);
            hashSet.Add(4);

            foreach (var item in hashSet)
            {
                Console.WriteLine(item);
            }

            IEnumerable enumerable = new int[] { 3, 4, 5 };
            foreach (var item in enumerable)
            {
                Console.WriteLine(item);
            }
            enumerable = new List<int>() { 3, 66, 4 };
            foreach (var item in enumerable)
            {
                Console.WriteLine(item);
            }

            var stack = new Stack<int>();
            stack.Push(2);
            stack.Push(23);
            stack.Push(22);
            enumerable = stack;
            

            foreach (var item in enumerable)
            {
                Console.WriteLine(item);
            }
            enumerable = new ArrayList() { 2, 454, 56 };
            foreach (var item in enumerable)
            {
                Console.WriteLine(item);
            }

            PrintAllElements(numbersList);
            PrintAllElements(stack);

            Console.ReadLine();
        }

        public static void PrintAllElements(IEnumerable enumerable)
        {
            foreach (var item in enumerable)
            {
                Console.WriteLine(item);
            }
        }
    }

    
}
