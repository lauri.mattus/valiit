﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            short a = 1000;
            int b;
            
            // Implicit conversion
            // toimub iseenesest teisendus
            b = a;


            Console.WriteLine(a);
            Console.WriteLine(b);

            // Explicit conversion
            // pead ise teisendama (cast)
            short c = (short)b;

            // Convert -> teisendad eri tüüpide (tekst ja number) vahel
            // string -> int

            // Cast -> teisendus (number ja number)
            // short -> int c
            // int -> long
            Console.WriteLine(c);

            b = 35000;
            c = (short)b;
            Console.WriteLine(c);

            // int + short = int
            // int + long = long
            // int * long = long
            // short / byte = int
            // byte / short = int

            // Matemaatilistel tehetel kahe eri numbri tüübi vahel
            // on tulemus alati suurem tüüp
            // välja arvatud tüübid, mis on väiksemad kui int
            // nende tehete tulemus on alati int

            byte d = 24;
            short e = 1000;
            
            Console.WriteLine(e / d);
            Console.WriteLine(d / e);

            Console.WriteLine(e / (d * b));


            uint f = 2000000000;
            uint g = 2000000000;

            ulong h = f * g;

            ushort i = 22;
            ushort j = 3;
            Console.WriteLine((i * j) + " " +(i * j).GetType());
            Console.ReadLine();

        }
    }
}
