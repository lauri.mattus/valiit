﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Loend või massiiv. Mingi hulk näiteks numbreid, sõnu vms
namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            // Luuakse täisarvude massiiv numbers, millesse
            // mahub 5 täisarvu
            // Loomise hetkel, pean määrama kui suure (mitu numbrit mahub) massiivi teen
            int[] numbers = new int[5];

            // Massiivi indeksid algavad 0'st.
            // Viimane indeks on alati 1 võrra väiksem 
            // kui Massiivi maksimum elementide arv 
            numbers[0] = 1;
            numbers[1] = 2;
            numbers[2] = -1;
            numbers[3] = -11;
            numbers[4] = 12;

            for (int i = 0; i < numbers.Length; i++)
            {
                
                Console.WriteLine("{0}. {1}", i, numbers[i]);
            }
             numbers[2] = -1;
            Console.WriteLine();

            // 2. Prindi tagurpidi
            for (int i = numbers.Length - 1; i >= 0 ; i--)
            {
                Console.WriteLine(numbers[i]);
            }

            Console.WriteLine();

            // 3. Prindi kõik positiivsed numbrid
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] > 0)
                {
                    Console.WriteLine(numbers[i]);
                }
            }

            // 4. Loo teine massiv 3le numbrile ja pane sinna esimese massiivi
            //    3 esimest numbrit. Prindi teise massiivi numbrid ekraanile
            int[] secondNumbers = new int[3];

          

            for (int i = 0; i < secondNumbers.Length; i++)
            {
                secondNumbers[i] = numbers[i];
            }

            Console.WriteLine();

            for (int i = 0; i < secondNumbers.Length; i++)
            {
                Console.WriteLine(secondNumbers[i]);
            }
            // 5. Loo kolmas massiv 3le numbrile ja pane sinna esimese massiivi
            //    3 numbrit tagant poolt alates. Prindi kolmanda massiivi numbrid ekraanile

            int[] thirdNumbers = new int[3];

            thirdNumbers[0] = numbers[4];
            thirdNumbers[1] = numbers[3];
            thirdNumbers[2] = numbers[2];

            for (int i = 0; i < thirdNumbers.Length; i++)
            {
                thirdNumbers[i] = numbers[numbers.Length - 1 - i];
            }

            int a = 4;
            for (int i = 0 ; i < thirdNumbers.Length; i++)
            {
                thirdNumbers[i] = numbers[a];
                a--;
            }

            for (int i = 0, j = 4; i < thirdNumbers.Length; i++, j--)
            {
                thirdNumbers[i] = numbers[j];
            }

            Console.WriteLine();

            for (int i = 0; i < thirdNumbers.Length; i++)
            {
                Console.WriteLine(thirdNumbers[i]);
            }

            Console.ReadLine();

        }
    }
}
