﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Cat : Pet
    {
        public int LivesRemaining { get; set; }

        public override string Name
        {
            get
            {
                return "dr. " +base.Name;
            }
        }

        public Cat(int age = 0)
        {
            Age = age;
        }

        public void CatchMouse()
        {
            Console.WriteLine("Püüan hiiri, sest ma kaalun väga palju tervelt {0}", weight);
        }

        public override void PrintInfo()
        {
            base.PrintInfo();

            Console.WriteLine("Elusi on järgi {0}", LivesRemaining);
        }
    }
}
