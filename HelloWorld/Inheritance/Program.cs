﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Pärinemine
// Põlvnemine
namespace Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat persian = new Cat();
            persian.Age = 2;
            persian.Breed = "Persian";
            persian.Name = "Miisu";
            persian.Color = "Grey";

            persian.CatchMouse();
            persian.Eat();

            persian.PrintInfo();

            Cat angora = new Cat()
            {
                Age = 3,
                Breed = "Angora",
                Name = "Kitu",
                Color = "White",
                LivesRemaining = 8
            };
            Console.WriteLine();

            angora.PrintInfo();

            Dog buldog = new Dog()
            {
                Name = "Naks",
                Age = 10,
                Breed = "Buldog",
                Color = "Black",
                IsChained = true
            };

            Console.WriteLine();

            buldog.Bark("ru");

            Console.WriteLine();

            buldog.PrintInfo();

            Rabbit rabbit = new Rabbit();
            rabbit.Name = "Juta";

            Lion lion = new Lion();
            lion.Hunt(rabbit);

            Wolf wolf = new Wolf();
            wolf.Hunt(rabbit);

            Console.WriteLine("Last hunted animal name {0}", lion.LastHuntedAnimal.Name);
            Console.WriteLine("Last hunted animal name {0}", wolf.LastHuntedAnimal.Name);

            
            Console.WriteLine(wolf);
            Console.WriteLine(angora);

            wolf.Eat();
            rabbit.Eat();
            angora.Eat();

            angora.FavouriteToy = "Pall";
            angora.OwnerName = "Kalle";
            angora.Name = "Pille";

            angora.PrintInfo();

            Console.ReadLine();
        }
    }
}
