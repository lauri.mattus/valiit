﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class DomesticAnimal : Animal
    {
        public string OwnerName { set; get; }

        public override void PrintInfo()
        {
            base.PrintInfo();
            Console.WriteLine("Omaniku nimi on {0}", OwnerName);
        }
        public override void Eat()
        {
            base.Eat();
        }
    }
}
