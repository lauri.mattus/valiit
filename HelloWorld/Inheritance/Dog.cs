﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Dog : Pet
    {
        private int yearOfBirth;

        public bool IsChained { get; set; }


        // Parameetriteta konstruktor
        // Vaikimisi on alati igal klassil üks tühi parameetriteta konstuktor
        // senikaua kui me uue konstuktori loome.
        // Kui ma loon näiteks uue 2 parameetriga konstuktori, siis kustutatakse 
        // vaikimisi nähtamatu parameetritega konstruktor ära

        public Dog()
        {
            Console.WriteLine("Loodi Dog objekt");
            // Vaikimisi kõik koerad on Matid
            Name = "Mati";

            yearOfBirth = DateTime.Now.Year;

            // Vaikimisi kõik koerad on 1 aastased
            Age = 1;
            // Vaikimisi kõik koerad on ketis
            IsChained = true;
            File.AppendAllText("dog.txt", "Loodi koer: " + Name + " " + DateTime.Now + "\r\n");

        }

        public Dog(string name = "Mati", int age = 0) 
        {
            File.AppendAllText("dog.txt", "Loodi koer: " + DateTime.Now + "\r\n");

            Name = name;
            Age = age;
            

        }

        public void Bark(string language)
        {
            if(language == "ru")
            {
                Console.WriteLine("gaf gaf");
            }
            else if(language == "en")
            {
                Console.WriteLine("Woof woof");
            }
            else
            {
                Console.WriteLine("Auh auh");
            }
           
        }
    }
}
