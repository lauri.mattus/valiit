﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadFromFile
{
    class Program
    {
        static void Main(string[] args)
        {
            
            // @ märgiga saab stringis lubada \ kasutuse
            string filePath = @"C:\Users\admin\Documents\GitHub\valiit\Input.txt";

            FileStream fileStream = new FileStream("C:\\Users\\admin\\Documents\\GitHub\\valiit\\Input.txt", FileMode.Open, FileAccess.Read);
            StreamReader streamReader = new StreamReader(fileStream);

            // Iga kord kui ma kutsun uuesti streamReader.ReadLine(), loetakse järgmine rida
            // Kui järgmist rida ei ole, siis streamReader.ReadLine() tagastab null
            string line = streamReader.ReadLine();

            while (line != null)
            {
                Console.WriteLine(line);
                line = streamReader.ReadLine();
            }
            
            streamReader.Close();
            fileStream.Close();

            Console.WriteLine(File.ReadAllText(filePath));
            string[] lines = File.ReadAllLines(filePath);
            for (int i = 0; i < lines.Length; i++)
            {
                Console.WriteLine(lines[i]);
            }

            Console.ReadLine();
        }
    }
}
