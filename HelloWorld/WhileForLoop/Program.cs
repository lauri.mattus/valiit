﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileForLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            //for (int i = 0; i < 5; i++)
            //{
            //    Console.WriteLine(i);
            //}

            int i = 1;
            while (i < 6)
            {
                Console.WriteLine(i);
                i++;
            }

            // Prindi ekraanile arvud 1 kuni 5

            // Küsi kasutajalt mis päev täna on
            // seni kuni ta ära arvab

            string answer = "";

            //while ("neljapäev" != answer.ToLower())
            for (; "neljapäev" != answer.ToLower(); )
            {
                Console.WriteLine("Mis päev täna on?");
                answer = Console.ReadLine();
            }

            Console.ReadLine();
        }
    }
}
