﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


// Exception on programmi töös esinev erijuht
// mille esinemisega ma peaks arvestama ja tegelema
namespace Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            // Try plokk eraldab koodi osa, 
            // kus me arvame, et võib juhtuda
            // exception
            try
            {
                string sentence = null;
              //  string[] words = sentence.Split(' ');
                Console.WriteLine("Sõnas on {0} sümboleid", sentence.Length);
                Console.WriteLine("Sõna oli {0}", sentence);
               
            }
            // Püüab kinni erandi
            // tüübist NullReferenceException
            // ehk selline tüüp erandist,
            // mis juhtub kui tahame kas mingit meetodit
            // või parameetit küsida milleltki, 
            // mis on parasjagu null
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Viga word on null: " +ex.Message);
            }

            int a = 3;
            int b = 8;
            Console.WriteLine("Arvude summa on {0}", a + b);

            a = 5;
            Console.WriteLine("Sisesta arv, millega tahad numbrit 5 jagada");


            try
            {
                b = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Arvude 5 ja {0} jagatis on {1}", b, a / b);
                //int[] numbers = new int[2];
                //numbers[2] = 3;
            }
            catch (DivideByZeroException ex)
            {
                Console.WriteLine("Nulliga jagada ei saa");
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Sisetatud väärtus ei olnud number");
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine("Indeks ei mahu õigesse vahe");
            }
            catch (Exception ex)
            {
                if (ex is FormatException || ex is DivideByZeroException)
                {
                    Console.WriteLine("Vales formaadis number või nulliga jagamine");
                }
                else
                {
                    Console.WriteLine("Juhtus mingi muu erand: {0} ", ex.Message);
                }
            }
            finally
            {
                Console.WriteLine("Finaal");
            }

            //catch (SystemException ex)
            //{
            //    Console.WriteLine("Süsteemi erand");
            //}

            // Püüa exception kinni ja kirjuta kasutajale: Nulliga ei saa jagada

            Console.ReadLine();
          
        }
    }
}
