﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringFunctions
{
    class Program
    {
        static void Main(string[] args)
        {
            string sentence = "Elas metsas Mutionu";

            Console.WriteLine("Lauses on {0} sümbolit", sentence.Length);

            // Leia esimese tühiku asukoht (index)
            int spaceIndex = sentence.IndexOf(" ");
            Console.WriteLine("Esimese tühiku indeks on {0}", spaceIndex);

            // Leia esimesest 3 sümbolist koosnev tekstiosa
            string subString = sentence.Substring(0, 3);
            Console.WriteLine("Esimesest 3 sümbolist koosnev tekstiosa {0}", subString);

            string sentenceWithSpaces = "";
            for (int i = 0; i < sentence.Length; i++)
            {
                sentenceWithSpaces += sentence[i];
                if(i != sentence.Length - 1)
                {
                    sentenceWithSpaces += " ";
                }
            }

            sentenceWithSpaces = "     " + sentenceWithSpaces + "    ";
            Console.WriteLine("\"{0}\"", sentenceWithSpaces);
            Console.WriteLine("\"{0}\"", sentenceWithSpaces.Trim());

            string[] words = new string[] { "Põdral", "maja", "metsa", "sees" };
        
            string joinedString = string.Join(" ", words);

            
            Console.WriteLine(joinedString);

            string[] splitWords = joinedString.Split(' ');

            for (int i = 0; i < splitWords.Length; i++)
            {
                Console.WriteLine(splitWords[i]);
            }

            // Küsi kasutajalt nimeiri arvudest, nii, et ta eraldab need tühikuga
            // Liida need kõik arvud kokku ja kirjuta vastus
            Console.WriteLine("Kirjuta nimekiri arvudest, eraldadades tühikuga");
            string answer = Console.ReadLine();
            string[] elements  = answer.Split(' ');

            int sum = 0;

            for (int i = 0; i < elements.Length; i++)
            {
                Console.WriteLine(elements[i]);
                sum += Convert.ToInt32(elements[i]);
            }

            int a = 3;
            int b = 2;

            Console.WriteLine("Arvude {0} ja {1} Summa on {2}", a+b, b, sum);
            Console.WriteLine($"Arvude {a+b} ja {b} Summa on {sum}");

            // Loo numbrite massiiv ja täida
            // see numbritega massiivist elements


            int[] numbers = new int[elements.Length];

            for (int i = 0; i < elements.Length; i++)
            {
                numbers[i] = Convert.ToInt32(elements[i]);
            }

            // prindi ekraanile järgnev rida
            // elements massiivist => "2 + 4 + 6 + 8 = 20"
            Console.WriteLine(string.Join(" + ", numbers) + " = " + sum);
            Console.WriteLine("{0} = {1}", string.Join(" + ", numbers), sum);

            Console.Write(string.Join(" + ", numbers));
            Console.Write(" = ");
            Console.Write(sum);


            Console.ReadLine();
        }
    }
}
