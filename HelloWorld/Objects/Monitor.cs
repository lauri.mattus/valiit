﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects
{
    // Enum on tüüp, kus saab defineerida erinevad valikud
    // Kasutakase siis, kui valikud ei muutu programmi töötamise jooksul
    // Tegelikult salvestatakse enumid alati int'na
    enum ScreenType
    {
        LCD, TFT, OLED, AMOLED
    }

    enum  Weekday { Monday, Tuesday, Wednesday }

    enum Color
    {
        Blue = 1, Grey = 2, Black = 3, White = 4, Red = 5
    }
    enum ScreenSize
    {
        Large = 65, Medium = 32, Small = 22
    }

    class Monitor
    {
        // privaat muutujad
        // field
        private Color color;
        // in "
        private double diagonal;

        private string manufacturer;
        private ScreenType screenType;
        private ScreenSize screenSize;

        public ScreenSize ScreenSize
        {
            get
            {
                return screenSize;
            }
            set
            {
                diagonal = (int)value;
                screenSize = value;
            }
        }

        public double Diagonal
        {
            get
            {
                return diagonal;
            }
        }

        public ScreenType ScreenType
        {
            get
            {
                return screenType;
            }
            set
            {
                screenType = value;
            }
        }

        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }


        // Property
        // get ja set meetodeid nimetatkse accessors
        public string Manufacturer
        {
            get
            {
                if(manufacturer == "Tootja puudub")
                {
                    Console.WriteLine("Kahjus on sellel montoril tootja seadistamata");
                    Console.WriteLine("Palun kirjuta tootja");
                    manufacturer = Console.ReadLine();
                }
                return manufacturer;
            }
            set

            {
                if (value == "Huawei")
                {
                    Console.WriteLine("Sellise tootja monitore meil pole");
                    manufacturer = "Tootja puudub";
                }
                else
                {// value tähendab seda väärtust, mis parasjagu panen proporty väärtuseks
                    manufacturer = value;
                }
            }
        }

        // Kapseldamine tähendab, et klass kontrollib millistele muutujatele 
        // lubab väljaspoolt ligi pääseda (Teised klassid)
        // Encapsulation


    }
}
