﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace Objects
{
    class Program
    {
        static void Main(string[] args)
        {
            // Loon klassist Monitor ühe eksmplari ehk objekti nimega monitor


            Monitor lgMonitor = new Monitor();
            
            lgMonitor.Manufacturer = "LG";
            lgMonitor.Color = Color.Black;
            lgMonitor.ScreenType = ScreenType.OLED;
            lgMonitor.ScreenSize = ScreenSize.Small;

            Console.WriteLine("Montori tootja on {0}, värv on {1}, diagonaal on {2}," +
                " ekraani tüüp on {3} ja ekraani suurus on {4}",
                lgMonitor.Manufacturer, lgMonitor.Color, lgMonitor.Diagonal, lgMonitor.ScreenType, lgMonitor.ScreenSize);

            // Keela kasutajal diagonaali seadistamine, luba ainult küsida
            // Kui kasutaja seadistab ekraani suuruse, siis seadista automaatselt selle järgi ka diagonaal


            int diagonal = (int)ScreenSize.Large;

            

            ScreenSize screenSize = (ScreenSize)65;
            Console.WriteLine(screenSize);

            Monitor sonyMonitor = new Monitor();
            sonyMonitor.Color = Color.White;
            sonyMonitor.Manufacturer = "Sony";
            sonyMonitor.ScreenSize = ScreenSize.Large;
            sonyMonitor.ScreenType = ScreenType.TFT;



            Monitor hpMonitor = new Monitor();
            hpMonitor.Color = Color.Blue;
            hpMonitor.ScreenSize = ScreenSize.Medium;
            hpMonitor.ScreenType = ScreenType.OLED;
            hpMonitor.Manufacturer = "HP";

            Console.WriteLine("Montori tootja on {0}, värv on {1}, diagonaal on {2}," +
                " ekraani tüüp on {3} ja ekraani suurus on {4}",
                sonyMonitor.Manufacturer, sonyMonitor.Color, sonyMonitor.Diagonal, sonyMonitor.ScreenType, sonyMonitor.ScreenSize);

            Console.WriteLine("Montori tootja on {0}, värv on {1}, diagonaal on {2}," +
                " ekraani tüüp on {3} ja ekraani suurus on {4}",
                hpMonitor.Manufacturer, hpMonitor.Color, hpMonitor.Diagonal, hpMonitor.ScreenType, hpMonitor.ScreenSize);

            // Tee massiiv 3 monitorist
            // prindi välja kõigi nende monitore tootjad, mille diangonaal on suurem kui 22"
            Monitor[] monitors = new Monitor[] { lgMonitor, sonyMonitor, hpMonitor };
            monitors[0] = lgMonitor;
            monitors[1] = sonyMonitor;
            monitors[2] = hpMonitor;

            // prindi kõigi monitoride värvid
            for (int i = 0; i < monitors.Length; i++)
            {
                Console.WriteLine(monitors[i].Color);
            }

            Console.WriteLine();

            foreach (Monitor monitor in monitors)
            {
                Console.WriteLine(monitor.Color);
            }

            Console.WriteLine();

            foreach (Monitor monitor in monitors)
            {
                if(monitor.Diagonal > 22)
                {
                    Console.WriteLine(monitor.Manufacturer);
                }
            }

            Console.ReadLine();
        }
    }
}
