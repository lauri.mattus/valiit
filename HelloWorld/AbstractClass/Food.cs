﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClass
{
    // Abstraktne klass on klassi ja interface'i hübriid,
    // milles on võimalik kirjedada ära nii meetodi
    // nimi, tagastustüüp ja parameetrid (nagu interface)
    // aga, saab ka kirjelda terveid meetodeid koos sisuga
    // Nii nagu interface'st ei saa ka abstract klassist otse objekti teha
    abstract class Food
    {
        // protected laseb muutujale ligi nii klassi seest, kui ka 
        // pärinevates klassides
        protected int calories = 500;

        // Erinevus interfacega
        // public abstract läheb ette
        public abstract void GoOff();
        public virtual int GetCalories()
        {
            return calories;
        }
    }
}
