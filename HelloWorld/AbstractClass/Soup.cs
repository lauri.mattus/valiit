﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClass
{
    class Soup : Food
    {
        // Abstract meetodi ülekirjutamine
        public override void GoOff()
        {
            Console.WriteLine("Supp läks pahaks");
        }

        public override int GetCalories()
        {
            return calories / 2;
        }
    }
}
