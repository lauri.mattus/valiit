﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElseIf
{
    class Program
    {
        static void Main(string[] args)
        {
            // Küsi kasutajalt arv1
            // Küsi kasutajalt arv2
            Console.WriteLine("Sisesta esimene arv");
            int firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Sisesta teine arv");
            int secondNumber = Convert.ToInt32(Console.ReadLine());

            if(firstNumber == 0  && secondNumber == 0)
            {
                Console.WriteLine("Mõlemad arvud on 0");
            }
            else if (firstNumber != 0 && secondNumber != 0 && firstNumber == secondNumber)
            {
                Console.WriteLine("Arvud on võrdsed");
            }
            else if (firstNumber < secondNumber)
            {
                Console.WriteLine("Esimene arv on väiksem");
            }
            else
            {
                Console.WriteLine("Teine arv on väiksem");
            }

            Console.ReadLine();
            // kaks arvu on võrdsed, prindi tekst arvud on võrdsed
            // kui esimene arv on suurem kui teine, prindi, et esimene on suurem
            // muul juhul prindi, et teine on suurem
        }
    }
}
