﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace VarKeyword
{
    enum Gender { Male, Female }

    class Program
    {
      

        static void Main(string[] args)
        {
            var b = 3;
            // var määrab ise tüübi
            // Täisarvu puhul pannakse väärtuseks int
            // või kui ei mahu pannakse uint
            // ja kui ikka ei mahu pannakse long
            int a = 3; // int
            var c = 3.0; // double
            var d = 3000000000; // uint
            var e = 5000000000; // long
            var f = 10000000000000000000; // ulong
            Console.WriteLine(a.GetType());
            Console.WriteLine(b.GetType());
            Console.WriteLine(c.GetType());
            Console.WriteLine(d.GetType());
            Console.WriteLine(e.GetType());
            Console.WriteLine(f.GetType());

            var g = "3"; // string
            var h = '3'; // char
            var i = 3f; // float
            var j = 3.0d; // double
            var k = 3.0m; // decimal

            var l = Convert.ToInt32("3");
            var m = Convert.ToDouble("3,0");

            var numbers = new []  { 1, 2, 3 };
            var words = new[] { "1", "2", "3"};
            var doubles = new int[3];

            var fileStream = new FileStream("minufail.txt", FileMode.Append, FileAccess.Write);

            var gender = Gender.Female;
            
            Console.ReadLine();
        }
    }
}
