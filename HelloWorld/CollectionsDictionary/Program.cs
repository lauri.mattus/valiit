﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsDictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            // "Auto" => "Car"
            // "Maja" => "House"
            // "Kass" => "Cat"
            // "Koer" => "Dog"

            Dictionary<string, string> dictionaryEn = new Dictionary<string, string>();
            dictionaryEn.Add("auto", "Car");
            dictionaryEn.Add("maja", "House");
            dictionaryEn.Add("kass", "Cat");
            dictionaryEn.Add("koer", "Dog");

            Console.WriteLine("Keys:");

            foreach (string item in dictionaryEn.Keys)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();
            Console.WriteLine("Values:");

            foreach (string item in dictionaryEn.Values)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            Dictionary<string, string> dictionaryDe = new Dictionary<string, string>();
            dictionaryDe.Add("auto", "Auto");
            dictionaryDe.Add("maja", "Haus");
            dictionaryDe.Add("kass", "Katze");
            dictionaryDe.Add("koer", "Hund");

            Console.WriteLine("Mis keelde soovid sõna tõlkida? inglise/saksa");
            string language = Console.ReadLine();


            Console.WriteLine("Mis sõna soovid tõlkida?");
            Console.WriteLine("Tunnen sellised sõnu: {0}", string.Join(", ", dictionaryEn.Keys));
            string word = Console.ReadLine().ToLower();

            if(language == "inglise")
            {
                Console.WriteLine("Sõna {0} {1} keeles on {2}", word, language, dictionaryEn[word]);
            }
            else
            {
                Console.WriteLine("Sõna {0} {1} keeles on {2}", word, language, dictionaryDe[word]);
            }

            Dictionary<string, string> countryCodes = new Dictionary<string, string>();
            countryCodes.Add("EE", "Estonia");
            countryCodes.Add("GER", "Germany");
            countryCodes.Add("USA", "United States Of America");

            // Asenda  "United States Of America" => "United states"
            countryCodes["USA"] = "United states";

            // Küsi kasutajalt riigikood ja sellele vastav riigi nimetus
            // Kui selline riigikood on olemas, siis asenda riik
            // Aga kui sellist riigikoodi veel ei olnud, siis lisa

            Console.WriteLine("Sisesta kood");
            string code = Console.ReadLine();

            Console.WriteLine("Sisesta riik");
            string country = Console.ReadLine();

            if (countryCodes.ContainsKey(code))
            {
                countryCodes[code] = country;
            }
            else
            {
                countryCodes.Add(code, country);
            }

            foreach (var item in countryCodes)
            {
                Console.WriteLine("{0} => {1}", item.Key, item.Value);
            }

            "Peeter".Split(new string[] { ", " }, StringSplitOptions.None);

            Console.ReadLine();
        }
    }
}
