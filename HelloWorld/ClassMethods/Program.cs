﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            // Car klassist objekti loomine
            var car = new Car();
            car.Model = "525";
            car.Make = "BMW";
            car.Year = 1777;

            car.StartEngine();
            car.StartEngine();

            if(car.IsEngineRunning)
            {
                Console.WriteLine("Auto hetkel töötab");
            }

            Console.WriteLine("Auto kiirus on hetkel {0}", car.Speed);
            car.Accelerate(200);

            Console.WriteLine("Auto kiirus on hetkel {0}", car.Speed);
            car.SlowDown(50);
            Console.WriteLine("Auto kiirus on hetkel {0}", car.Speed);

            Person driver = new Person();
            driver.FirstName = "Kalle";
            driver.LastName = "Kaalikas";
            driver.Age = 25;
            driver.Gender = Gender.Male;

            Person owner = new Person()
            {
                FirstName = "Malle",
                LastName = "Maasikas",
                Age = 30,
                Gender = Gender.Female
            };

            car.Driver = driver;
            car.Owner = owner;

            // Prindi välja auto omaniku perekonnanimi
            Console.WriteLine("Auto {0} omaniku perekonnanimi on {1}",
                car.Make, car.Owner.LastName);

            var list = new List<int>() { 1, 2, 3 };

            var passengers = new List<Person>()
            {
                new Person()
                {
                    FirstName = "Jüri",
                    LastName = "Juurikas",
                    Age = 20
                },
                new Person()
                {
                    FirstName = "Peeter",
                    LastName = "Peet",
                    Age = 27
                },
                new Person()
                {
                    FirstName = "Toomas",
                    LastName = "Toomingas",
                    Age = 22
                }
            };

            
         

            car.Passengers = passengers;

            for (int i = 0; i < car.Passengers.Count; i++)
            {
                   Console.WriteLine("{0}. reisia vanus on {1}", i + 1, car.Passengers[i].Age);
            }

            foreach (var passenger in car.Passengers)
            {
                Console.WriteLine(passenger.Age);
            }

            Console.WriteLine("Reisijate vanused on: {0}, {1}, {2}",
                car.Passengers[0].Age, car.Passengers[1].Age, car.Passengers[2].Age);

            // Lisa autosse 3 reisijat ja prindi välja nende vanused
            // Lisa autojuhile ema ja isa ning autojuhi emale ja isale lisa mõlemale isa

            // ning küsi auto objekti käest mis on selle autojuhi mõlemate vanaisade
            // eesnimed
            car.Driver.Father = new Person()
            {
                FirstName = "Roomet",
                Father = new Person()
                {
                    FirstName = "Jaan"
                },
                Mother = owner
            };
            car.Driver.Mother = new Person()
            {
                FirstName = "Tiiu",
                Father = new Person()
                {
                    FirstName = "Juhan"
                }
            };

            // Autojuhi vanaisa nime muutmine
            car.Driver.Father.Father.FirstName = "Peeter";

            Console.WriteLine(car.Driver.Father.Father.FirstName);
            Console.WriteLine(car.Driver.Mother.Father.FirstName);

            // Prindi välja autojuhi kõikide meesliini esiisade nimed

            car.Driver.Father.Father.Father = new Person
            {
                FirstName = "John",
                Father = new Person
                {
                    FirstName = "Jake"
                }
            };

            // Roomet, Peeter, John, Jake

            Console.WriteLine();

            Person.PrintFatherName(car.Driver);

            //string.Join(" ", new string[] { "a", "b" });
            //string sentence = "Juku ja Juhan";
            //sentence.Split(' ');
            
            Console.ReadLine();
        }
    }
}
