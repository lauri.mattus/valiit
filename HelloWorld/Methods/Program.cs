﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            PrintHello();
            PrintHello();
            PrintHello();

            PrintText("Tere kuidas elad?");
            PrintText("Elan hästi!");

            int a = 3;
            PrintHello(a);
            

            PrintText("Tere", 4);

            PrintText();

            PrintText("Tere", 4, false);
            PrintText("");
            PrintText("Tere", 4, true);

            PrintText("Tere", 4);

            Console.ReadLine();
        }

        static void PrintHello()
        {
            Console.WriteLine("Hello");
        }

        //static void PrintText(string text)
        //{
        //    Console.WriteLine(text);
        //}

        static void PrintHello(int howManyTimes)
        {
            howManyTimes++;
            for (int i = 0; i < howManyTimes; i++)
            {
                PrintHello();
            }
        }


        // Veel üks meetod PrintText,
        // mis lisaks tekstile ja korduste arvule omab kolmandat parameetrit
        // mis määrab ära, 
        // kas need korduvad tekstid prinditakse ühele või  mitmele reale
        static void PrintText(string text = "", int howManyTimes = 1, bool oneLine = false)
        {
            for (int i = 0; i < howManyTimes; i++)
            {
                if(oneLine)
                {
                    Console.Write(text + " ");
                }
                else
                {
                    Console.WriteLine(text);
                }
            }
        }
    }
}
