﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoWhileLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            //bool exitProgram = true;

            //while (!exitProgram)
            //{
            //    Console.WriteLine("Tere");
            //    Console.WriteLine("Kas soovid jätkata? j/e");
            //    if(Console.ReadLine() != "j")
            //    {
            //        exitProgram = true;
            //    }
            //}


            // Do while tsükkel erineb tavalisest while tsüklist selle poolest,
            // et esimene tsükli kordus tehakse alati, olenemata kas kontrollitav tingimus on 
            // tõene või vale

            // Kasutatakse siis, kui mingi tegevus on tehtud ning tahan otsustada, kas teen seda veel
            do
            {
                Console.WriteLine("Tere");
                Console.WriteLine("Kas soovid jätkata? j/e");

            } while (Console.ReadLine() == "j");
        }
    }
}
