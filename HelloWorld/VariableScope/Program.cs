﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VariableScope
{
    class Program
    {
        // Kõik muutujad, mis ma defineerin kehtivad/elavad
        // {} piires või sees

        static void Main(string[] args)
        {
            int a = 4;

            Increment(a);
            Console.WriteLine("Value of a is {0}", a);

            string sentence = "Mis sa teed";
            AddQuestionMark(sentence);

            Console.WriteLine("Value of sentence is {0}", sentence);

            int[] integers = new int[] { 2, 4, 23, -11, 12 };

            PrintNumbers(integers);
            Increment(integers);

            Console.WriteLine();

            PrintNumbers(integers);

            int b = 3;
            int c = b;
            b = 7;

            Console.WriteLine();
            Console.WriteLine(c);

            int[] secondIntegers = integers;
            secondIntegers[0] = -7;
            integers[1] = 22;

            Console.WriteLine(integers[0]);
            Console.WriteLine(secondIntegers[1]);

            string firstWord = "raud";
            string secondWord = firstWord;

            firstWord = firstWord + "tee";
            Console.WriteLine(secondWord);
            Console.WriteLine(firstWord);

            List<int> numbers = new List<int>() { 2, -4, 0 };
            List<int> secondNumbers = numbers;

            numbers.RemoveAt(2);

            Console.WriteLine();

            for (int i = 0; i < secondNumbers.Count; i++)
            {
                Console.WriteLine(secondNumbers[i]);
            }

            Console.ReadLine();

        }

        // Kui meetodile anda parameetriks kaasa lihttüüpi 'Value Type' muutuja väärtus,
        // siis tegelikult tehakse uus muutuja ja pannakse sinna sama väärtus
        // Kui meetodile anda parameetriks kaasa Reference Type muutuja.
        // siis antakse kaasa tegelikult sama muutuja (uus muutuja, mis viitab samale mäluaadressile)


        // Value Type - väärtusmuutuja: int, char, string, double, float, decimal, bool, short, long
        // Reference Type - viitmuutuja: massiivid, listid, dictionary, kõik klassid

        
        static void Increment(int b)
        {
            b++;
            Console.WriteLine("New number is {0}", b);
        }

        static void Increment(int[] numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                //numbers[i] = numbers[i] + 1;
                numbers[i]++;
            }
        }

        static void PrintNumbers(int[] numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine(numbers[i]);
            }
        }

        static void AddQuestionMark(string sentence)
        {
            sentence = sentence + "?";
            Console.WriteLine("New sentence is {0}", sentence);
        }


    }
}
