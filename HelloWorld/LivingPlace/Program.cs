﻿using Inheritance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LivingPlace
{
    class Program
    {
        static void Main(string[] args)
        {
            ILivingPlace livingPlace = new Forest();
            livingPlace.AddAnimal(new Cow());
            livingPlace.AddAnimal(new Cow());
            livingPlace.AddAnimal(new Cow());
            livingPlace.AddAnimal(new Cow());
            livingPlace.AddAnimal(new Cow());
            livingPlace.AddAnimal(new Cow());
            livingPlace.AddAnimal(new Pig());
            livingPlace.AddAnimal(new Pig());
            livingPlace.AddAnimal(new Pig());

            livingPlace.PrintAnimals();
            livingPlace.RemoveAnimal("Pig");
            livingPlace.RemoveAnimal("Pig");
            livingPlace.RemoveAnimal("Horse");

            Console.WriteLine();
            livingPlace.PrintAnimals();

            var count = livingPlace.GetAnimalCount("Horse");
            if (count != 0)
            {
                Console.WriteLine("Hobuseid on {0}", count);
            }

            livingPlace.AddAnimal(new Wolf());

            Console.ReadLine();
        }
    }
}
