﻿using Inheritance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivingPlace
{
    public class Farm : ILivingPlace
    {
        private List<FarmAnimal> animals = new List<FarmAnimal>();
        private Dictionary<string, int> animalCounts = new Dictionary<string, int>();

        public int MaxAnimalCount { get; private set; }

        public Farm(int count = 25)
        {
            MaxAnimalCount = count;
        }

        // 1. Lisada loomi
        public void AddAnimal(Animal animal)
        {
            // is kontrollib kas võrreldav objekt animal pärineb või on tüübist FarmAnimal
            if(!(animal is FarmAnimal))
            {
                Console.WriteLine("Farmis saavad elada ainult farmi loomad");
                return;
            }
            if(animals.Count >= MaxAnimalCount)
            {
                Console.WriteLine("Farmis on juba maksimum kogus loomi");
                return;
            }
            if(!animalCounts.ContainsKey(animal.GetType().Name)
                && animalCounts.Count >= 4)
            {
                Console.WriteLine("Farmis on juba 4 erinevat looma");
                return;
            }
            if (animalCounts.ContainsKey(animal.GetType().Name)
                && animalCounts[animal.GetType().Name] >= 5)
            {
                Console.WriteLine("Farmis on juba 5 {0}", animal.GetType().Name);
                return;
            }

            animals.Add((FarmAnimal)animal);
            Console.WriteLine("{0} lisati Farmi", animal.GetType().Name);

            // Uue looma lisamine
            // animalCounts.Count näitab alati mitu erinevat looma mul on
            if (!animalCounts.ContainsKey(animal.GetType().Name))
            {
                animalCounts.Add(animal.GetType().Name, 1);
                return;
            }
            animalCounts[animal.GetType().Name]++;
            //wordCounts[word] = wordCounts[word] + 1;
            //wordCounts[word] += 1;

        }
        // 2. Küsida mis loomad on
        // prindib looma tüübi koos arvuga
        // Cow 4
        // Pig 2
        public void PrintAnimals()
        {
            foreach (var animalCount in animalCounts)
            {
                Console.WriteLine("{0} {1}", animalCount.Key, animalCount.Value);
            }
        }

        // 3. Küsida konkreese looma arvu
        public int GetAnimalCount(string animalType)
        {
            if(animalCounts.ContainsKey(animalType))
            {
                return animalCounts[animalType];
            }
            Console.WriteLine("Looma {0} ei leitud", animalType);
            return 0;
        }


        // 4. Eemaldada loomi
        public void RemoveAnimal(string animalType)
        {
            if(!animalCounts.ContainsKey(animalType))
            {
                Console.WriteLine("Looma {0} ei leitud", animalType);
                return;
            }
            //animals.Remove(FarmAnimal animal)
            //animals.RemoveAt(int index)
            for (int i = 0; i < animals.Count; i++)
            {
                if (animals[i].GetType().Name == animalType)
                {
                    //animals.RemoveAt(i);
                    animals.Remove(animals[i]);
                    Console.WriteLine("{0} eemaldati Farmist", animalType);
                    // Kui on viimane loom ehk dictionarys on kogus on 1
                    // siis eemalda dictionaryst
                    if (animalCounts[animalType] == 1)
                    {
                        animalCounts.Remove(animalType);
                    }
                    // muul juhul vähenda kogust 1 võrra
                    else
                    {
                        animalCounts[animalType]--;
                    }
                    break;
                }
            }
        }

        // 5. Täienda koodi nii, et farmis saab olla 4 erinevat looma ja igat erinevat looma olla 5 tükki
        // 6. Täienda RemoveAnimal nii, et kui sellist looma ei leitud, siis prindib "Looma ei leitud"
        // 7. Täienda GetAnimalCount(string animalType) nii, et kui sellist looma ei leitud, siis prindib "Looma ei leitud"
        // ja tagastab 0
        // 8. Looge klassid Forest ja Zoo ja mõelge, kuidas teha interface ILivingPlace, kus on kirjas kõik ühine
        // Farm, Zoo ja Forest jaoks
    }
}
