﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inheritance;

namespace LivingPlace
{
    public class Forest : ILivingPlace
    {
        private List<Animal> animals = new List<Animal>();
        private Dictionary<string, int> animalCounts = new Dictionary<string, int>();

        public int MaxAnimalCount
        {
            get
            {
                return int.MaxValue;
            }
        }

        public void AddAnimal(Animal animal)
        {
            animals.Add(animal);
            Console.WriteLine("{0} lisati metsa", animal.GetType().Name);
            
            // Uue looma lisamine
            if (!animalCounts.ContainsKey(animal.GetType().Name))
            {
                animalCounts.Add(animal.GetType().Name, 1);
                return;
            }
            animalCounts[animal.GetType().Name]++;
        }

        public int GetAnimalCount(string animalType)
        {
            if (animalCounts.ContainsKey(animalType))
            {
                return animalCounts[animalType];
            }
            Console.WriteLine("Looma {0} ei leitud", animalType);
            return 0;
        }

        public void PrintAnimals()
        {
            foreach (var animalCount in animalCounts)
            {
                Console.WriteLine("{0} {1}", animalCount.Key, animalCount.Value);
            }
        }

        public void RemoveAnimal(string animalType)
        {
            if (!animalCounts.ContainsKey(animalType))
            {
                Console.WriteLine("Looma {0} ei leitud", animalType);
                return;
            }
            //animals.Remove(FarmAnimal animal)
            //animals.RemoveAt(int index)
            for (int i = 0; i < animals.Count; i++)
            {
                if (animals[i].GetType().Name == animalType)
                {
                    //animals.RemoveAt(i);
                    animals.Remove(animals[i]);
                    Console.WriteLine("{0} eemaldati metsast", animalType);
                    // Kui on viimane loom ehk dictionarys on kogus on 1
                    // siis eemalda dictionaryst
                    if (animalCounts[animalType] == 1)
                    {
                        animalCounts.Remove(animalType);
                    }
                    // muul juhul vähenda kogust 1 võrra
                    else
                    {
                        animalCounts[animalType]--;
                    }
                    break;
                }
            }
        }
    }
}
