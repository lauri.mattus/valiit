﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            // Küsi kasutajalt arvud, millega tahad tehteid teha
            // eralda arvud tühikuga (küsi ühe reaga)
            Console.WriteLine("Sisesta arvud, millega tahad tehteid teha.");
            Console.WriteLine("Eralda arvud tühikuga");

            string[] elements = Console.ReadLine().Split(' ');
            
            int[] numbers = new int[elements.Length];

            for (int i = 0; i < elements.Length; i++)
            {
                numbers[i] = Convert.ToInt32(elements[i]);
            }

            // olenevalt sellest, kas kasutaja sisestas 2, 3 või enam arvu
            // paku kasutajale tehted, mida kasutaja saab nende arvudega teha

            Console.WriteLine("Vali tehe");
            switch (numbers.Length)
            {
                case 1:
                    Console.WriteLine("Tehte jaoks on vaja vähemalt 2 numbrit");
                    break;
                case 2:
                    {
                        Console.WriteLine("a) Liida");
                        Console.WriteLine("b) Lahuta");
                        Console.WriteLine("c) Korruta");
                        Console.WriteLine("d) Jaga");
                        string answer = Console.ReadLine();
                        switch (answer)
                        {
                            case "a":
                                Console.WriteLine("Arvude {0} ja {1} summa on {2}", numbers[0], numbers[1], Sum(numbers));
                                break;
                            case "b":
                                Console.WriteLine("Arvude {0} ja {1} vahe on {2}", numbers[0], numbers[1], Subtract(numbers[0], numbers[1]));
                                break;
                            case "c":
                                Console.WriteLine("Arvude {0} ja {1} korrutis on {2}", numbers[0], numbers[1], Multiply(numbers[0], numbers[1]));
                                break;
                            default:
                                Console.WriteLine("Arvude {0} ja {1} jagatis on {2}", numbers[0], numbers[1], Divide(numbers[0], numbers[1]));
                                break;
                        }

                        break;
                    }

                case 3:
                    {
                        Console.WriteLine("a) Liida");
                        Console.WriteLine("b) Korruta");
                        string answer = Console.ReadLine();
                        switch (answer)
                        {
                            case "a":
                                Console.WriteLine("Arvude {0}, {1} ja {2} summa on {3}", numbers[0], numbers[1], numbers[2], Sum(numbers));
                                break;
                            default:
                                Console.WriteLine("Arvude {0}, {1} ja {2} korrutis on {3}", numbers[0], numbers[1], numbers[2], Multiply(numbers[0], numbers[1], numbers[2]));
                                break;
                        }

                        break;
                    }

                default:
                    {
                        Console.WriteLine("a) Liida");
                        string answer = Console.ReadLine();
                        if (answer == "a")
                        {
                            // Arvude 3, 4, 64, -23, 33, 200 ja -17 summa on 352362
                            List<int> numbersList = numbers.ToList();
                            numbersList.RemoveAt(numbersList.Count - 1);

                            Console.WriteLine(string.Join(", ", elements, 0, numbers.Length - 1));
                            Console.WriteLine("Arvude {0} ja {1} summa on {2}", string.Join(", ", numbersList), numbers[numbers.Length - 1], Sum(numbers));
                            Console.WriteLine("Arvude {0} ja {1} summa on {2}", string.Join(", ", numbersList), numbers[numbers.Length - 1], Sum(numbers));
                        }

                        break;
                    }
            }

            // kui panna 2 arvu
            // Vali tehe
            // a) Liida
            // b) Lahuta
            // c) Korruta
            // d) Jaga


            //int sum = Sum(2, 6);
            //Console.WriteLine("Arvude 2 ja 6 summa on {0}", sum);
            //Console.WriteLine("Arvude 2, 6 ja 10 summa on {0}", Sum(2, 6, 10));

            //Console.WriteLine("Arvude 2, 6 korrutis on {0}", Multiply(2, 6));
            //Console.WriteLine("Arvude 2, 6 ja 10 korrutis on {0}", Multiply(2, 6, 10));

            //double answer = Divide(2, 6);

            //Console.WriteLine("Arvude 2 ja 6 jagatis on {0}", answer);

            //int[] numbers = new int[] { 1, 2, -4, 8 };
            //sum = Sum(numbers);
            //Console.WriteLine("Arvude summa on {0}", sum);

            Console.ReadLine();
        }

        static int Sum(int a, int b, int c = 0)
        {
            int sum = a + b + c;
            return sum;
        }

        static int Subtract(int a, int b)
        {
            return a - b;
        }

        static int Multiply(int a, int b, int c = 1)
        {
            return a * b * c;
        }

        static double Divide(int a, int b)
        {
            return a / (double)b;
        }

        static int Sum(int[] numbers)
        {
            int sum = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                sum = sum + numbers[i];
                // sum += numbers[i]
            }
            return sum;
        }
        // Subtract
        // Multiply
        // Divide

        // Muuda korrutamise ja liitmise meetodid nii, et toimivad ka 3 numbriga
        // Loo meetod, mis liidab kõik masiivi numbrid kokku
    }
}
