﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountWords
{
    class Program
    {
        static void Main(string[] args)
        {
            // Küsi kasutajalt lause
            // ja loe kokku kõik erinevad sõnad ja prindi välja
            // mitu korda need sõnad esinevad

            // "Mina ja Pets läksime tööle ja siis koju ja siis tööle jälle"
            // Mina 1
            // ja 3
            // Pets 1
            // läksime 1
            // siis 2
            // koju 1
            // tööle 2
            // jälle 1

            Dictionary<string, int> wordCounts = new Dictionary<string, int>();

            Console.WriteLine("Sisesta lause");
            string sentence = Console.ReadLine();

            string[] words = sentence.Split(' ');

            foreach (string word in words)
            {
                if (!wordCounts.ContainsKey(word))
                {
                    wordCounts.Add(word, 1);
                }
                else
                {
                    //wordCounts[word] = wordCounts[word] + 1;
                    //wordCounts[word] += 1;
                    wordCounts[word]++;

                }
            }

            foreach (var item in wordCounts)
            {
                Console.WriteLine("{0} {1}", item.Key, item.Value);
            }

            Console.ReadLine();

        }
    }
}
