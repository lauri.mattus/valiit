﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Text;
using System.Threading.Tasks;

namespace ConsoleArguments
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1. Sõna
            // 2. Mitu korda tahad sõna printida
            // Kui kastuaja ei ole 2 parameetrit sisestanud
            // prinditakse kasutajale 
            if(args.Length == 2  && int.TryParse(args[1], out int count))
            {
                for (int i = 0; i < count; i++)
                {
                    Console.WriteLine(args[0]);
                }
            }
            else
            {
                Console.WriteLine("Kasutus: {0} [sõna] [mitu korda]", System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
            }

        }
    }
}
