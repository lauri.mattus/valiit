﻿using System;

namespace Nullables
{
    class Program
    {
        static void Main(string[] args)
        {
            // Annab võimaluse eristada olukorda, kas mul lihttüübil on väärtus antud
            // või on vaikeväärtus
            int b;
            b = 0;

            int? a = 3;
            a = null;
            a = -4;
            a = null;

            bool? attendedSchool = false;

            attendedSchool = null;

            //  if(attendedSchool != null)
            if (attendedSchool.HasValue)
            {
                //Console.WriteLine("Tema väärtus on {0}", attendedSchool);
                Console.WriteLine("Tema väärtus on {0}", attendedSchool.Value);
            }
            else
            {
                Console.WriteLine("On ilma väärtuseta");
            }

            DateTime? date = null;
            
        }
    }
}
