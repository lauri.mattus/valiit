﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Main thread started");
            Task.Run( () => DoWork());
            Console.WriteLine("Main thread done");
           
            Console.ReadLine();
        }

        static async Task DoWork()
        {
            Console.WriteLine("Starting work");
            for (int i = 1; i < 6; i++)
            {
                Thread.Sleep(2000);
                Console.WriteLine(i);
            }
            await Task.Run(() => DoMoreWork());
            Console.WriteLine("Work done");
        }

        static void DoMoreWork()
        {
            Console.WriteLine("Starting more work");
            for (int i = -1; i > -6; i--)
            {
                Thread.Sleep(2000);
                Console.WriteLine(i);
            }
            Console.WriteLine("More work done");
        }
    }
}
