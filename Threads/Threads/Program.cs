﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Threads
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Main thread started");

            Thread threadA = new Thread(DoWork);
            threadA.Start();

            Thread.Sleep(2000);
            Console.WriteLine("Main thread continues");
            Thread threadB = new Thread(DoWork);
            threadB.Start();
           
            
            Console.WriteLine("Mis on sinu nimi?");
            var name = Console.ReadLine();
            Console.WriteLine("Tere {0}", name);

            // Main thread ootab seni kuni threadA on lõpetanud
            threadA.Join();
            threadB.Join();

            Console.WriteLine("Main thread done");
            Console.ReadLine();

        }

        static void DoWork()
        {
            Console.WriteLine("Starting work");
            for (int i = 1; i < 6; i++)
            {
                Thread.Sleep(2000);
                Console.WriteLine(i);
            }
            Console.WriteLine("Work done");
        }
    }
}
