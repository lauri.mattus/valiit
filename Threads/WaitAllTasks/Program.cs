﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WaitAllTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Task taskA = Task.Run(() => DoWork());
            Task taskB = Task.Run(() => DoMoreWork());
            Task taskC = Task.Run(() => DoWorkAndSum());

            Task.WaitAll(taskA, taskB, taskC);

            Console.WriteLine("All tasks finished");
            Console.ReadLine();
        }

        static async Task DoWorkAndSum()
        {
            int count = await DoWork();
            Console.WriteLine("{0} jobs done", count);
        }

        static async Task<int> DoWork()
        {
            int count = 5;
            Console.WriteLine("Starting work");
            for (int i = 1; i <= count; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine(i);
            }
            await DoMoreWork();
            Console.WriteLine("Work done");
            return count;
        }

        static async Task DoMoreWork()
        {
            Console.WriteLine("Starting more work");
            for (int i = -1; i > -6; i--)
            {
                Thread.Sleep(1000);
                Console.WriteLine(i);
            }
            await Task.Run(() => DoEvenMoreWork());
            Console.WriteLine("More work done");
        }

        static void DoEvenMoreWork()
        {
            Console.WriteLine("Starting even more work");
            for (int i = 1; i < 6; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine("+" + i);
            }
            Console.WriteLine("Even more work done");
        }
    }
}
