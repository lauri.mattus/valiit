﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singletons
{
    class Program
    {
        static void Main(string[] args)
        {
            if(2 == 2)
            {
                Student firstStudent = new Student() { FirstName = "Malle", LastName = "Maasikas" };
                Student secondStudent = new Student() { FirstName = "Jaan", LastName = "Jalgratas" };

            }
            Student thirdStudent = new Student() { FirstName = "Peeter", LastName = "Peet" };

            Console.WriteLine(Student.Count);

            Singleton singleton = Singleton.Instance;
            singleton.Age = 20;

            Singleton secondSignleton = Singleton.Instance;
            Console.WriteLine(secondSignleton.Age);

            Console.ReadLine();
        }
    }
}
