namespace DatabaseFirst
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model2 : DbContext
    {
        public Model2()
            : base("name=Model2")
        {
        }

        public virtual DbSet<Author> Author { get; set; }
        public virtual DbSet<AuthorBook> AuthorBook { get; set; }
        public virtual DbSet<Book> Book { get; set; }
        public virtual DbSet<Loan> Loan { get; set; }
        public virtual DbSet<LoanType> LoanType { get; set; }
        public virtual DbSet<Student> Student { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Author>()
                .HasOptional(e => e.Author1)
                .WithRequired(e => e.Author2);

            modelBuilder.Entity<Book>()
                .HasMany(e => e.AuthorBook)
                .WithRequired(e => e.Book)
                .WillCascadeOnDelete(false);
        }
    }
}
