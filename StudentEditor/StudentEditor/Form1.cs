﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace StudentEditor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonInsert_Click(object sender, EventArgs e)
        {
            string firstName = textBoxFirstName.Text;
            string lastName = textBoxLastName.Text;
            int age = Convert.ToInt32(textBoxAge.Text);

            string connectionString = "Server = ILEKTOR; Database = AspNetCore; Trusted_Connection = True";
            Insert(firstName, lastName, age, connectionString);
        }

        private static void Insert(string firstName, string lastName, int age, string connectionString)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            string sql = "INSERT INTO Student " +
                    "(FirstName, LastName, Age) " +
                 "VALUES " +
                    "(@FirstName, @LastName, @Age)";

            SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@FirstName", firstName);
            command.Parameters.AddWithValue("@LastName", lastName);
            command.Parameters.AddWithValue("@Age", age);

            int linesChanged = command.ExecuteNonQuery();
            MessageBox.Show("Changes count: " + linesChanged);

            connection.Close();
        }
    }
}
